<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dosen extends Model
{
    protected $table="dosem";
 	protected $primaryKey="id";
 	protected $fillable=['nip','nama_lengkap','gelar','riwayat pendidikan'];
}
