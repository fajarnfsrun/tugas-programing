<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
 
class companyController extends Controller
{
    public function index()
    {
    	
    	$employer = DB::table('employer')->get();
 
    	// mengirim data company ke view index
    	return view('index',['employer' => $employer]);
 
    }
}