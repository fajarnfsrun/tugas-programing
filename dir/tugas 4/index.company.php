<html>
<head>
	<title>company</title>
</head>
<body>
	<h3>Data company</h3>
 
	<a href="/company/tambah">Tambah company Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>id</th>
			<th>nama</th>
			<th>atasan_id</th>
			<th>company_id</th>
			
			<th>Opsi</th>
		</tr>
		@foreach($company as $p)
		<tr>
			<td>{{ $p->company_id }}</td>
			<td>{{ $p->company_nama }}</td>
			<td>{{ $p->company_atasan_id }}</td>
			<td>{{ $p->company_company_id }}</td>
			
			<td>
				<a href="/company/edit/{{ $p->company_id }}">Edit</a>
				|
				<a href="/company/hapus/{{ $p->company_id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
 
 
</body>
</html>