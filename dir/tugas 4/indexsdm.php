html>
<head>
	<title>data pegawai</title>
</head>
<body>
	<h3>Data Pegawai</h3>
 
	<a href="/pegawai/tambah">Tambah Pegawai Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>id</th>
			<th>nama</th>
			<th>jenis kelamin</th>
			<th>status</th>
			<th> tanggal lahir</th>
			<th> tanggal masuk</th>
			<th> kodedepartemen</th>
			<th>Opsi</th>
		</tr>
		@foreach($sdm as $p)
		<tr>
			<td>{{ $p->sdm_id }}</td>
			<td>{{ $p->sdm_nama }}</td>
			<td>{{ $p->sdm_jenis kelamin }}</td>
			<td>{{ $p->sdm_status }}</td>
			<td>{{ $p->sdm_tanggal lahir }}</td>
			<td>{{ $p->sdm_tanggal masuk }}</td>
			<td>{{ $p->sdm_kodedepartemen}}</td>
			<td>
				<a href="/pegawai/edit/{{ $p->sdm_id }}">Edit</a>
				|
				<a href="/pegawai/hapus/{{ $p->sdm_id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
 
 
</body>
</html>