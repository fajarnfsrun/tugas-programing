<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
 
class sdmController extends Controller
{
    public function index()
    {
    	
    	$sdm = DB::table('sdm')->get();
 
    	// mengirim data pegawai ke view index
    	return view('index',['sdm' => $sdm]);
 
    }
}