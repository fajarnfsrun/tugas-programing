-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2020 at 01:34 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sdm`
--

-- --------------------------------------------------------

--
-- Table structure for table `departemen`
--

CREATE TABLE `departemen` (
  `id` int(5) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `departemen`
--

INSERT INTO `departemen` (`id`, `nama`) VALUES
(1, 'Manajemen'),
(2, 'Pengembangan Bisnis'),
(3, 'teknisi'),
(4, 'Analis');

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `id` int(5) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis kelamin` varchar(2) NOT NULL,
  `status` varchar(21) NOT NULL,
  `tanggal lahir` date NOT NULL,
  `tanggal masuk` date NOT NULL,
  `departemen` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama`, `jenis kelamin`, `status`, `tanggal lahir`, `tanggal masuk`, `departemen`) VALUES
(1, 'Rizky Saputra', 'L', 'menikah', '1980-10-11', '2011-01-01', '1'),
(2, 'Farhan Reza', 'L', 'belum', '1989-11-01', '2011-01-01', '1'),
(3, 'Riyando Adi', 'L', 'menikah', '1977-01-25', '2011-01-01', '1'),
(4, 'Diego Manuel', 'L', 'menikah', '1983-02-22', '2012-09-04', '2'),
(5, 'Satya Laksana', 'L', 'Menikah', '1981-01-12', '2011-03-19', '2'),
(6, 'Miguel Hernandez', 'L', 'menikah', '1981-10-16', '2014-06-15', '2'),
(7, 'Putri Persada', 'P', 'menikah', '1988-01-30', '2013-04-14', '2'),
(8, 'Alma Safira', 'P', 'Menikah', '1991-06-03', '2013-09-28', '3'),
(9, 'Haqi Hafiz', 'L', 'Belum', '1995-09-19', '2015-03-09', '3'),
(10, 'Abi Isyawara', 'L', 'Belum', '1991-06-03', '2012-01-22', '3'),
(11, 'Maman Kresna', 'L', 'belum', '1993-08-21', '2012-09-15', '3'),
(12, 'Nasia Aulia', 'P', 'Belum', '1989-10-07', '2012-05-07', '4'),
(13, 'Mutiara Rezki', 'P', 'Menikah', '1988-03-23', '2013-05-21', '4'),
(14, 'Dani Setiawan', 'L', 'Belum', '1986-02-11', '2014-11-30', '4'),
(15, 'Budi Putra', 'L', 'Belum', '1995-10-23', '2015-12-03', '4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
